## SpringBoot/Java8 Rest application for products

 - build: mvn clean install
 - test: mvn clean test
 
## Considerations
 - Application was required to be used JAX-RS while I've just notice now after
 writing most of the code. I will let as it is but I understand this risks the 
 project evaluation. Also I would to delay more time then the expected.
 - Application expects products optionally points to its parent only and not list of children on creation API. 
 I.e Do not send children products.
 - MockMVC test are not going to be delivered from controllers. Maybe just for few APIs for skills showing. Focused in general logic.
 - I don't know wheter 'image' should be treated as binary information so used simple Entity.
 If need to check some skills in that sense pls look: https://github.com/NicolasFonte/file-uploader
 
## API features:

  - Create, update and delete products. See: (createProduct{please don't send 'children' here}, updateProduct, deleteProduct)
  - Create, update and delete images. See: (createImage, updateImage, deleteImage)
  - Get all products excluding relationships (child products, images). See getProducts
  - Get all products including specified relationships (child product and/or images). See: getProductsFlat (set flags)
  - Same as 3 using specific product identity See. getProduct.
  - Same as 4 using specific product identity See. getProductFlat (set Flags)
  - Get set of child products for specific product. See getProductChildren
  - Get set of images for specific product. See getProductImages