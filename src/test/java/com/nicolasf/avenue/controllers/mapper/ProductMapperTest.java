package com.nicolasf.avenue.controllers.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import com.nicolasf.avenue.domainobject.Image;
import com.nicolasf.avenue.domainobject.Product;
import com.nicolasf.avenue.domainvalue.ImageDTO;
import com.nicolasf.avenue.domainvalue.ProductDTO;
import java.util.Collections;
import java.util.List;
import org.junit.Test;

public class ProductMapperTest {

    @Test
    public void testFromDataTransfer() throws Exception {
        List<ImageDTO> images = Collections.singletonList(ImageDTO.builder().name("imageName").build());

        ProductDTO productDTO = ProductDTO.builder()
                .name("product")
                .parentId(1L)
                .id(2L)
                .images(images)
                .build();

        Product product = ProductMapper.fromDataTransfer(productDTO);

        assertThat(product)
                .isNotNull()
                .extracting(Product::getName, product1 -> product1.getImages().get(0).getName())
                .contains("product", "imageName");
    }

    @Test
    public void testFromEntity() throws Exception {
        Product product = new Product();
        product.setId(2L);
        product.setName("product");
        product.setImages(Collections.singletonList(new Image("imageName", 15)));
        Product productChild = new Product();
        productChild.setName("productChild");
        product.setChildrenProducts(Collections.singletonList(productChild));

        ProductDTO productDTO = ProductMapper.fromEntity(product, true, true);

        assertThat(productDTO)
                .isNotNull()
                .extracting(ProductDTO::getName,
                        prod -> prod.getImages().get(0).getName(),
                        prod -> prod.getImages().get(0).getSize(),
                        prod -> prod.getChildrenProducts().get(0).getName())
                .contains("product", "imageName", 15, "productChild");
    }

}