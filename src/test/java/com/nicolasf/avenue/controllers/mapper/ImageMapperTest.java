package com.nicolasf.avenue.controllers.mapper;

import static org.junit.Assert.*;

import com.nicolasf.avenue.domainobject.Image;
import com.nicolasf.avenue.domainvalue.ImageDTO;
import java.util.Arrays;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.assertj.core.groups.Tuple;
import org.junit.Test;

public class ImageMapperTest {

    @Test
    public void testFromDataTransfer() throws Exception {
        ImageDTO imageDTO = ImageDTO.builder()
                .id(1L)
                .name("image")
                .size(15)
                .build();

        Image image = ImageMapper.fromDataTransfer(imageDTO);

        Assertions.assertThat(image)
                .extracting("name", "size")
                .contains("image", 15);
    }

    @Test
    public void testFromEntity() throws Exception {
        Image image = new Image();
        image.setId(1L);
        image.setSize(15);
        image.setName("image");

        ImageDTO imageDTO = ImageMapper.fromEntity(image);

        Assertions.assertThat(imageDTO)
                .isNotNull()
                .extracting("size", "name")
                .contains(15, "image");
    }

    @Test
    public void testFromEntities() throws Exception {
        Image image1 = new Image();
        image1.setSize(15);
        image1.setName("image1");

        Image image2 = new Image();
        image2.setSize(20);
        image2.setName("image2");

        List<ImageDTO> imageDTOs = ImageMapper.fromEntities(Arrays.asList(image1, image2));

        Assertions.assertThat(imageDTOs)
                .hasSize(2)
                .extracting("size", "name")
                .contains(Tuple.tuple(15, "image1"), Tuple.tuple(20, "image2"));
    }

}