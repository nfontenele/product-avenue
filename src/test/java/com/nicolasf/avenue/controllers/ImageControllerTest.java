package com.nicolasf.avenue.controllers;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nicolasf.avenue.controllers.mapper.ImageMapper;
import com.nicolasf.avenue.domainobject.Image;
import com.nicolasf.avenue.domainvalue.ImageDTO;
import com.nicolasf.avenue.service.ImageService;
import java.io.IOException;
import java.nio.charset.Charset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ImageControllerTest {

    private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;

    @Mock
    private ImageService imageService;

    @Before
    public void setUp() {
        ImageController imageController = new ImageController(imageService);
        mockMvc = standaloneSetup(imageController).build();
    }

    @Test
    public void testAPICreateImage() throws Exception {
        ImageDTO imageDTO = ImageDTO.builder()
                .id(1L)
                .name("image1")
                .size(15)
                .build();
        when(imageService.create(anyObject())).thenReturn(ImageMapper.fromDataTransfer(imageDTO));

        mockMvc.perform(post("/v1/api/products/images")
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(imageDTO)).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("image1")))
                .andExpect(jsonPath("$.size", is(15)));

    }

    @Test
    public void testAPIDeleteImage() throws Exception {
        mockMvc.perform(delete("/v1/api/products/images/1")
                .contentType(APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());

        verify(imageService).delete(1L);
    }

    @Test
    public void testAPIUpdateImage() throws Exception {
        when(imageService.update(1L, "image", 20)).thenReturn(new Image("image", 20));

        mockMvc.perform(put("/v1/api/products/images/1?name=image&size=20")
                .contentType(APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("image")))
                .andExpect(jsonPath("$.size", is(20)));

    }

    public byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

}