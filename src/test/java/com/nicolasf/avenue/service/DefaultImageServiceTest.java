package com.nicolasf.avenue.service;

import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.nicolasf.avenue.domainobject.Image;
import com.nicolasf.avenue.repository.ImageRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class DefaultImageServiceTest {

    @Mock
    private ImageRepository imageRepository;

    private DefaultImageService imageService;

    @Before
    public void setUp() {
        imageService = new DefaultImageService(imageRepository);
    }

    @Test
    public void testImageCreated() throws Exception {
        Image image = new Image();

        imageService.create(image);

        verify(imageRepository, only()).save(image);
    }

    @Test
    public void testImageDeleted() throws Exception {
        imageService.delete(1L);

        verify(imageRepository, only()).delete(1L);
    }

    @Test
    public void testImageUpdate() throws Exception {
        Image image = new Image();
        image.setId(1L);
        image.setName("name");
        image.setSize(15);
        when(imageRepository.findOne(1L)).thenReturn(image);

        imageService.update(1L, "nameUpdated", 20);

        ArgumentCaptor<Image> argument = ArgumentCaptor.forClass(Image.class);
        verify(imageRepository).save(argument.capture());
        Assertions.assertThat(argument.getValue().getName()).isEqualTo("nameUpdated");
        Assertions.assertThat(argument.getValue().getSize()).isEqualTo(20);
    }

}