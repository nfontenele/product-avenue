package com.nicolasf.avenue.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.nicolasf.avenue.domainobject.Image;
import com.nicolasf.avenue.domainobject.Product;
import com.nicolasf.avenue.exception.EntityNotFoundException;
import com.nicolasf.avenue.repository.ProductRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class DefaultProductServiceTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private ProductRepository productRepository;

    private DefaultProductService productService;

    @Before
    public void setUp() throws Exception {
        productService = new DefaultProductService(productRepository);
    }

    @Test
    public void testFindAllProducts() throws Exception {
        productService.findAll();

        verify(productRepository).findAll();
    }

    @Test
    public void testProductFind() throws Exception {
        Product product = new Product();
        product.setId(1L);
        when(productRepository.findOne(1L)).thenReturn(product);

        Product productFound = productService.find(1L);

        assertThat(productFound)
                .isNotNull()
                .extracting("id")
                .contains(1L);
    }

    @Test
    public void testProductNotFoundThrowsEntityException() throws Exception {
        when(productRepository.findOne(1L)).thenReturn(null);

        thrown.expectMessage("Product: 1 not found.");
        thrown.expect(EntityNotFoundException.class);

        productService.find(1L);

    }

    @Test
    public void testDeleteProduct() throws Exception {
        productService.delete(1L);

        verify(productRepository).delete(1L);
    }

    @Test
    public void testProductUpdated() throws Exception {
        Product product = new Product();
        product.setId(1L);
        product.setName("name");
        when(productRepository.findOne(1L)).thenReturn(product);

        productService.update(1L, "nameUpdated");

        ArgumentCaptor<Product> argument = ArgumentCaptor.forClass(Product.class);
        verify(productRepository).save(argument.capture());
        Assertions.assertThat(argument.getValue().getName()).isEqualTo("nameUpdated");

    }

    @Test
    public void testProductCreated() throws Exception {
        Product parent = new Product();
        parent.setId(1L);
        when(productRepository.findOne(1L)).thenReturn(parent);

        Product product = new Product();
        product.setName("product");
        product.setId(2L);
        product.setParent(parent);

        productService.create(product);

        verify(productRepository).save(product);
    }

}