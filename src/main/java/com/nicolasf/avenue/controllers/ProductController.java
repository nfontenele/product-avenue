package com.nicolasf.avenue.controllers;

import com.nicolasf.avenue.controllers.mapper.ImageMapper;
import com.nicolasf.avenue.domainobject.Product;
import com.nicolasf.avenue.domainvalue.ImageDTO;
import com.nicolasf.avenue.exception.EntityNotFoundException;
import com.nicolasf.avenue.controllers.mapper.ProductMapper;
import com.nicolasf.avenue.domainvalue.ProductDTO;
import com.nicolasf.avenue.service.ProductService;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = ProductController.PRODUCT_URL)
public class ProductController {

    static final String PRODUCT_URL = "/v1/api/products";

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProductDTO createProduct(@RequestBody @Valid ProductDTO productDTO) throws EntityNotFoundException {
        Product product = productService.create(ProductMapper.fromDataTransfer(productDTO));
        return ProductMapper.fromEntity(product, false, true);

    }

    @PutMapping("{id}")
    public ProductDTO updateProduct(@PathVariable Long id, @RequestParam String name) throws EntityNotFoundException {
        Product updatedProduct = productService.update(id, name);
        return ProductMapper.fromEntity(updatedProduct, false, false);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Long id) {
        productService.delete(id);
    }

    @GetMapping
    public List<ProductDTO> getProducts() {
        return productService.findAll()
                .stream()
                .map(product -> ProductMapper.fromEntity(product, false, false))
                .collect(Collectors.toList());
    }

    @GetMapping("/flat")
    public List<ProductDTO> getProductsFlat(
            @RequestParam(defaultValue = "true") Boolean hasChildren,
            @RequestParam(defaultValue = "true") Boolean hasImages) {
        return productService.findAll()
                .stream()
                .map(product -> ProductMapper.fromEntity(product, hasChildren, hasImages))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ProductDTO getProduct(@PathVariable Long id) throws EntityNotFoundException {
        return ProductMapper.fromEntity(productService.find(id), false, false);

    }

    @GetMapping("/{id}/flat")
    public ProductDTO getProductFlat(
            @PathVariable Long id,
            @RequestParam(defaultValue = "true") Boolean hasChildren,
            @RequestParam(defaultValue = "true") Boolean hasImages) throws EntityNotFoundException {
        return ProductMapper.fromEntity(productService.find(id), hasChildren, hasImages);
    }

    @GetMapping("/{id}/children")
    public List<ProductDTO> getProductChildren(@PathVariable Long id) throws EntityNotFoundException {
        Product product = productService.find(id);
        return ProductMapper.fromEntities(product.getChildrenProducts(), false, true);
    }

    @GetMapping("/{id}/images")
    public List<ImageDTO> getProductImages(@PathVariable Long id) throws EntityNotFoundException {
        Product product = productService.find(id);
        return product.getImages().stream()
                .map(ImageMapper::fromEntity)
                .collect(Collectors.toList());

    }

}
