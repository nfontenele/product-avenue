package com.nicolasf.avenue.controllers;

import com.nicolasf.avenue.controllers.mapper.ImageMapper;
import com.nicolasf.avenue.domainobject.Image;
import com.nicolasf.avenue.domainvalue.ImageDTO;
import com.nicolasf.avenue.exception.EntityNotFoundException;
import com.nicolasf.avenue.service.ImageService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Url mapped as a 'sub-resource' of product controller.
 */
@RestController
@RequestMapping(value = ProductController.PRODUCT_URL + "/images")
public class ImageController {

    private final ImageService imageService;

    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ImageDTO createImage(@RequestBody ImageDTO imageDTO) throws EntityNotFoundException {
        Image image = imageService.create(ImageMapper.fromDataTransfer(imageDTO));
        return ImageMapper.fromEntity(image);

    }

    @PutMapping("{id}")
    public ImageDTO updateImage(@PathVariable Long id,
            @RequestParam String name,
            @RequestParam Integer size) throws EntityNotFoundException {
        Image updatedImage = imageService.update(id, name, size);
        return ImageMapper.fromEntity(updatedImage);
    }

    @DeleteMapping("/{id}")
    public void deleteImage(@PathVariable Long id) {
        imageService.delete(id);
    }
}
