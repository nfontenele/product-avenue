package com.nicolasf.avenue.controllers.mapper;

import com.nicolasf.avenue.domainobject.Image;
import com.nicolasf.avenue.domainobject.Product;
import com.nicolasf.avenue.domainvalue.ProductDTO;
import com.nicolasf.avenue.domainvalue.ProductDTO.ProductDTOBuilder;
import java.util.List;
import java.util.stream.Collectors;

public class ProductMapper {

    /**
     * For now never set product children from client. Just add new product and set parent.
     */
    public static Product fromDataTransfer(ProductDTO productDTO) {
        Product product = new Product();
        product.setName(productDTO.getName());

        if (productDTO.getParentId() != null && productDTO.getParentId() != 0L) {
            Product parent = new Product();
            parent.setId(productDTO.getParentId());
            product.setParent(parent);
        }

        if (productDTO.getImages() != null) {
            List<Image> images = productDTO.getImages()
                    .stream()
                    .map(ImageMapper::fromDataTransfer)
                    .collect(Collectors.toList());
            product.setImages(images);
        }

        return product;
    }

    public static ProductDTO fromEntity(Product product, boolean hasChildren, boolean hasImages) {
        ProductDTOBuilder dtoBuilder = ProductDTO.builder()
                .id(product.getId())
                .name(product.getName());

        if (hasChildren && product.getChildrenProducts() != null) {
            dtoBuilder.childrenProducts(fromEntities(product.getChildrenProducts(), false, false));
        }

        if (hasImages && product.getImages() != null) {
            dtoBuilder.images(ImageMapper.fromEntities(product.getImages()));
        }

        return dtoBuilder.build();
    }

    public static List<ProductDTO> fromEntities(List<Product> products, boolean hasChildren, boolean hasImages) {
        return products.stream()
                .map(product -> fromEntity(product, hasChildren, hasImages))
                .collect(Collectors.toList());
    }
}
