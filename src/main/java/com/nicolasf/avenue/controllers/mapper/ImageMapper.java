package com.nicolasf.avenue.controllers.mapper;

import com.nicolasf.avenue.domainobject.Image;
import com.nicolasf.avenue.domainvalue.ImageDTO;
import java.util.List;
import java.util.stream.Collectors;

public class ImageMapper {

    public static Image fromDataTransfer(ImageDTO imageDTO) {
        Image image = new Image();
        image.setName(imageDTO.getName());
        image.setSize(imageDTO.getSize());

        return image;
    }

    public static ImageDTO fromEntity(Image image) {
        return ImageDTO.builder()
                .name(image.getName())
                .size(image.getSize())
                .id(image.getId())
                .build();
    }

    static List<ImageDTO> fromEntities(List<Image> images) {
        return images.stream()
                .map(ImageMapper::fromEntity)
                .collect(Collectors.toList());
    }


}
