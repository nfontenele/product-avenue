package com.nicolasf.avenue.repository;

import com.nicolasf.avenue.domainobject.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
