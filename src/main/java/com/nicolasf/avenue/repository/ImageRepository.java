package com.nicolasf.avenue.repository;

import com.nicolasf.avenue.domainobject.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Long> {

}
