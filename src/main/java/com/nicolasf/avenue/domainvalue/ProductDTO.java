package com.nicolasf.avenue.domainvalue;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductDTO {

    @JsonIgnore
    private Long id;

    @NotNull(message = "Name cannot be null;")
    private String name;

    private Long parentId;

    private List<ProductDTO> childrenProducts;

    private List<ImageDTO> images;

    private ProductDTO() {
    }
}
