package com.nicolasf.avenue.service;

import com.nicolasf.avenue.domainobject.Image;
import com.nicolasf.avenue.exception.EntityNotFoundException;
import java.util.List;

public interface ImageService {

    Image create(Image image) throws EntityNotFoundException;

    void delete(Long id);

    Image update(Long id, String name, Integer size) throws EntityNotFoundException;
}
