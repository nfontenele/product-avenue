package com.nicolasf.avenue.service;

import com.nicolasf.avenue.exception.EntityNotFoundException;
import com.nicolasf.avenue.domainobject.Product;
import com.nicolasf.avenue.repository.ProductRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class DefaultProductService implements ProductService {

    private final ProductRepository productRepository;

    public DefaultProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product find(Long id) throws EntityNotFoundException {
        Product product = productRepository.findOne(id);
        if (product == null) {
            throw new EntityNotFoundException("Product: " + id + " not found.");
        }
        return product;
    }

    @Override
    public Product create(Product product) throws EntityNotFoundException {
        if (product.getParent() != null) {
            Product parentProduct = find(product.getParent().getId());
            product.setParent(parentProduct);
        }
        return productRepository.save(product);
    }

    @Override
    public void delete(Long id) {
        productRepository.delete(id);
    }

    @Override
    public Product update(Long id, String name) throws EntityNotFoundException {
        Product product = productRepository.findOne(id);
        if (product == null) {
            throw new EntityNotFoundException("Product: " + id + " not found.");
        }
        product.setName(name);
        return productRepository.save(product);

    }
}
