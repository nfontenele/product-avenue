package com.nicolasf.avenue.service;

import com.nicolasf.avenue.exception.EntityNotFoundException;
import com.nicolasf.avenue.domainobject.Product;
import java.util.List;

public interface ProductService {

    List<Product> findAll();

    Product find(Long id) throws EntityNotFoundException;

    Product create(Product product) throws EntityNotFoundException;

    void delete(Long id);

    Product update(Long id, String name) throws EntityNotFoundException;
}
