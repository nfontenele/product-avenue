package com.nicolasf.avenue.service;

import com.nicolasf.avenue.domainobject.Image;
import com.nicolasf.avenue.exception.EntityNotFoundException;
import com.nicolasf.avenue.repository.ImageRepository;
import org.springframework.stereotype.Service;

@Service
public class DefaultImageService implements ImageService {

    private final ImageRepository imageRepository;

    public DefaultImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public Image create(Image image) throws EntityNotFoundException {
        return imageRepository.save(image);
    }

    @Override
    public void delete(Long id) {
        imageRepository.delete(id);
    }

    @Override
    public Image update(Long id, String name, Integer size) throws EntityNotFoundException {
        Image image = imageRepository.findOne(id);
        if (image == null) {
            throw new EntityNotFoundException("Image: " + id + " not found.");
        }
        image.setName(name);
        image.setSize(size);
        return imageRepository.save(image);
    }
}
